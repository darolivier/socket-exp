var app = require('express')();
var http = require('http').createServer(app);
var io = require('socket.io')(http);

app.get('/', function(req, res){
  res.sendFile(__dirname + '/index.html');
});

var clients =[];
var currentUser=""

io.on('connection', function(socket){
  
  console.log('a user connected');
  socket.join("myRoom")
  socket.on('monpseudo', (pseudo)=>{
    clients.push(pseudo);
    this.pseudo = currentUser
    io.emit('listUsers',clients)
    io.emit('addUser', (pseudo))
  })

  socket.on('invitation', (id, pseudo, currentUser)=>{
    console.log(currentUser)
    io.to(id).emit("askPlay", pseudo ,"Voulez-vous faire une partie avec", currentUser, "?")

  })

  socket.on('disconnect', function(){
      clients.splice(clients.indexOf(currentUser),1)
      io.emit('listUsers',clients)
      console.log("disconnected")
    });
})

http.listen(3000, function(){
  console.log('listening on *:3000');
});